# GPS Route Tracker for Android

## Features

- **Open Street Maps Integration:** Display maps within the app, adding a rich, navigable interface.
- **Route Tracking:** Record and display running or walking paths with GPS, marking the journey on the map.
- **Performance Metrics:** Calculate and show the time taken, distance covered, and average speed for each route.
- **Persistent Storage:** Save route information using the Room database library for later retrieval.
- **Background Location Service:** A dedicated service for obtaining GPS updates, ensuring accurate tracking even when the app is not in the foreground.
- **Customizable Settings:** Allows users to personalize the route color and adjust location update frequency.
- **Comprehensive Permissions Handling:** Learn to request and manage location permissions across different Android versions.

## Getting Started

### Prerequisites

- Android Studio
- Kotlin SDK
- An Android device or emulator with GPS capabilities

## Project Outcomes

- Integration of Open Street Maps in Android applications.
- Managing GPS data for route tracking.
- Implementing background services for location updates.
- Handling Android permissions for location services.
- Using the Room database for data persistence.
- Customizing application settings and preferences.
