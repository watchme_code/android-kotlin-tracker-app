package com.desarrollo_neco.gpstrackercurse

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.desarrollo_neco.gpstrackercurse.databinding.ActivityMainBinding
import com.desarrollo_neco.gpstrackercurse.fragments.MainFragment
import com.desarrollo_neco.gpstrackercurse.fragments.SettingsFragment
import com.desarrollo_neco.gpstrackercurse.fragments.TracksFragment
import com.desarrollo_neco.gpstrackercurse.utils.openFragment

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        onBottomNavClicks()
        openFragment(MainFragment.newInstance())
    }

    private fun onBottomNavClicks(){
        binding.bNan.setOnItemSelectedListener {
            when(it.itemId){
                R.id.id_home -> openFragment(MainFragment.newInstance())
                R.id.id_tracks -> openFragment(TracksFragment.newInstance())
                R.id.id_settings -> openFragment(SettingsFragment())
            }
            true
        }
    }
}