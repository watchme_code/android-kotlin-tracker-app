package com.desarrollo_neco.gpstrackercurse

import android.app.Application
import com.desarrollo_neco.gpstrackercurse.db.MainDb

class MainApp : Application() {
    val database by lazy { MainDb.getDatabase(this) }
}